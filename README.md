# Objective

Test [JavaFX](https://wiki.openjdk.java.net/display/OpenJFX/Main) usage in a small application.

# Task description

To compute the intersection of two lists A and B, the elements of one list
are put into a HashSet while the other list is iterated over to test for
each element whether it’s contained in the HashSet. If one list is significantly
larger than the other, which one would you put into the HashSet?

The application should provide a user interface which can be used to enter the
following parameters:

* Size of list A
* Size of list B
* Choose which list is put into a HashSet, which one is iterated over
* A Run-button to start the computation
* An output fields to show the size of the result-set
* A second output field to show the time it took to run the intersection algorithm

If the Run-button is pressed, the two lists are populated with random numbers.
Based on the user input, one of the lists is put into a HashSet, the other one is
iterated over to test for each element if it is contained in the HashSet. Matching
elements are put into the result set.

Please use JavaFX or Swing for the UI and Gradle or Maven as build tool.

# Solution

## Requirements

* jdk 11

## Testing
```
./gradlew test
```

## Usage
```
./gradlew run
```

## Todo / known limitations

* unit test `MainController`, see below
* move strings shown in the UI from controller to `.fxml` file
* move styling directives from `.fxml` to `.css`
* move objects creation to injector class or use DI framework
* the application does not handle nor prevents memory exhaustion in any particular way

### Unit testing JavaFX controller

Lessons learned

* JavaFX objects need to be run in context of JavaFX thread. It is easy to solve
by starting fake test application before the test, eg. in a static initializer
(see `AbstractJavaFxTest`), using junit test `@Rule` (see [gist](https://gist.github.com/andytill/3835914))
or by creation of junit test runner (see [gist](https://gist.github.com/bugabinga/a2703bfce0ca842d0e38))
* JavaFX node objects are full of final methods which cannot be mocked with _mockito_

Further possibilities

* use dedicated test library like [TestFX](https://github.com/TestFX/TestFX)
* use wrappers that can delegate calls to JavaFX nodes and can be mocked
* extract more logic from a controller and test it away from JavaFX
* use mocking library which allows mocking final methods
