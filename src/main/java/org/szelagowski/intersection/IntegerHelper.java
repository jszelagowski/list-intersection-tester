package org.szelagowski.intersection;

import java.util.Optional;

public class IntegerHelper {

	public static Optional<Integer> nonNegativeInt(String value) {
		return safeToInt(value.trim())
				.filter(v -> v >= 0);
	}

	private static Optional<Integer> safeToInt(String string) {
		try {
			return Optional.of(Integer.valueOf(string));
		} catch (NumberFormatException e) {
			return Optional.empty();
		}
	}
}
