package org.szelagowski.intersection;

import java.util.List;
import java.util.SplittableRandom;
import java.util.function.Consumer;

public class IntersectionTester {
	private static final SplittableRandom RANDOM = new SplittableRandom();

	private RandomHelper helper = new RandomHelper();
	private Intersector intersector = new Intersector();

	public Timing.MeasuredTime<List<Integer>> generateListsAndMeasureIntersectingTime(final int intoSetSize, final int iterateOverSize, final Consumer<State> notify) {
		notify.accept(State.GeneratingIntoSetList);
		final var intoSet = helper.randomList(intoSetSize, RANDOM::nextInt);
		notify.accept(State.GeneratingIterateOverList);
		final var iterateOver = helper.randomList(iterateOverSize, RANDOM::nextInt);
		notify.accept(State.Intersecting);
		return Timing.measure(() -> intersector.intersection(intoSet, iterateOver));
	}

	enum State {
		GeneratingIntoSetList, GeneratingIterateOverList, Intersecting;
	}
}
