package org.szelagowski.intersection;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

import java.net.URL;


public class IntersectionTestingApplication extends Application {

	@Override
	public void start(Stage primaryStage) throws Exception {
		final URL resource = getClass().getClassLoader().getResource("main.fxml");
		assert resource != null;
		Parent root = FXMLLoader.load(resource);
		primaryStage.setTitle("Check time of finding lists intersection");
		primaryStage.setScene(new Scene(root, 800, 500));
		primaryStage.show();
	}

	public static void main(String[] args) {
		Application.launch(args);
	}
}
