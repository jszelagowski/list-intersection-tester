package org.szelagowski.intersection;

import java.util.HashSet;
import java.util.List;
import java.util.stream.Collectors;

public class Intersector {

	public List<Integer> intersection(final List<Integer> intoSet, final List<Integer> iterateOver) {
		final var set = new HashSet<Integer>(intoSet);
		return iterateOver.stream().filter(set::contains).collect(Collectors.toList());
	}
}
