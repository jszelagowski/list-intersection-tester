package org.szelagowski.intersection;

import javafx.application.Platform;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.Node;
import javafx.scene.control.*;

import java.util.Collections;
import java.util.List;
import java.util.Optional;
import java.util.concurrent.CompletableFuture;
import java.util.stream.Stream;

import static org.szelagowski.intersection.IntegerHelper.nonNegativeInt;


public class MainController {
	@FXML
	private TextField sizeAField;
	@FXML
	private TextField sizeBField;
	@FXML
	private ToggleGroup whichListsRadioGroup;
	@FXML
	private RadioButton radioListA;
	@FXML
	private Button runButton;
	@FXML
	private Label infoLabel;

	private IntersectionTester intersectionTester = new IntersectionTester();

	@FXML
	public void initialize() {
		verifyNonEmptyInteger(sizeAField);
		verifyNonEmptyInteger(sizeBField);
	}

	@FXML
	public void measureIntersecting(ActionEvent event) {
		nonNegativeInt(sizeAField.getText()).ifPresent(sizeA ->
				nonNegativeInt(sizeBField.getText()).ifPresent(sizeB -> {
					forallFieldsSetDisabled(true);
					CompletableFuture.runAsync(() -> {
						final var result = isASelected()
								? intersectionTester.generateListsAndMeasureIntersectingTime(sizeA, sizeB, this::updateStatus)
								: intersectionTester.generateListsAndMeasureIntersectingTime(sizeB, sizeA, this::updateStatus);
						showResult(result);
						forallFieldsSetDisabled(false);
					});
				})
		);
	}

	private void verifyNonEmptyInteger(final TextField field) {
		field.textProperty().addListener((observable, oldValue, newValue) ->
				nonNegativeInt(field.getText()).ifPresentOrElse(ignore -> clearRed(field), () -> markRed(field))
		);
	}

	private void markRed(TextField field) {
		Optional.ofNullable(field.getParent().lookup(".label.error")).ifPresent(v -> v.setVisible(true));
		if (!field.getStyleClass().contains("error")) {
			field.getStyleClass().add("error");
		}
	}

	private void clearRed(TextField field) {
		Optional.ofNullable(field.getParent().lookup(".label.error")).ifPresent(v -> v.setVisible(false));
		field.getStyleClass().removeAll(Collections.singleton("error"));
	}

	private boolean isASelected() {
		return Optional.ofNullable((RadioButton) whichListsRadioGroup.getSelectedToggle())
				.filter(v -> v.equals(radioListA))
				.isPresent();
	}

	private void forallFieldsSetDisabled(Boolean value) {
		Stream.concat(Stream.of(sizeAField, sizeBField, runButton), whichListsRadioGroup.getToggles().stream().map(v1 -> (Node) v1))
				.forEach(v -> v.disableProperty().setValue(value));
	}

	private void updateStatus(IntersectionTester.State status) {
		switch (status) {
			case GeneratingIntoSetList:
				setInfoAsync("Generating list to put into the set...");
				break;
			case GeneratingIterateOverList:
				setInfoAsync("Generating list to iterate over...");
				break;
			case Intersecting:
				setInfoAsync("Intersecting...");
				break;
		}
	}

	private void showResult(Timing.MeasuredTime<List<Integer>> result) {
		setInfoAsync(String.format("The intersection size is %d\n" +
				"It took %d ms\n", result.value.size(), result.elapseMs));
	}

	private void setInfoAsync(String text) {
		Platform.runLater(() -> infoLabel.setText(text));
	}
}