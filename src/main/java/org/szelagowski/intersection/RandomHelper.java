package org.szelagowski.intersection;

import java.util.List;
import java.util.function.Supplier;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

public class RandomHelper {

	public <T> List<T> randomList(final int size, Supplier<T> generator) {
		return IntStream.range(0, size)
				.mapToObj(i -> generator.get())
				.collect(Collectors.toList());
	}
}
