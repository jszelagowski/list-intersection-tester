package org.szelagowski.intersection;

import java.util.function.Supplier;

public class Timing {
	static class MeasuredTime<T> {
		public final long elapseMs;
		public final T value;

		MeasuredTime(Long elapsedMs, T value) {
			this.elapseMs = elapsedMs;
			this.value = value;
		}
	}

	public static <T> MeasuredTime<T> measure(Supplier<T> f) {
		var startTime = System.nanoTime();
		var value = f.get();
		var elapsedMs = (System.nanoTime() - startTime) / 1000000;
		return new MeasuredTime<T>(elapsedMs, value);
	}
}
