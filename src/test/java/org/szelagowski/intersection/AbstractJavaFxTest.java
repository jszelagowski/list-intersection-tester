package org.szelagowski.intersection;

import javafx.application.Application;
import javafx.stage.Stage;
import junit.framework.TestCase;


abstract class AbstractJavaFxTest extends TestCase {

	public static class AsNonApp extends Application {
		@Override
		public void start(Stage primaryStage) throws Exception {
			// noop
		}
	}

	static {
		Thread t = new Thread("JavaFX Test Thread") {
			public void run() {
				Application.launch(AsNonApp.class);
			}
		};
		t.setDaemon(true);
		t.start();
	}
}