package org.szelagowski.intersection;

import junit.framework.TestCase;

import java.util.Optional;

public class IntegerHelperTest extends TestCase {

	public void testNonNegativeInteger() {
		assertEquals(Optional.of(0), IntegerHelper.nonNegativeInt("0"));
		assertEquals(Optional.of(1), IntegerHelper.nonNegativeInt("1"));
		assertEquals(Optional.of(4733), IntegerHelper.nonNegativeInt("4733"));
		assertEquals(Optional.of(Integer.MAX_VALUE), IntegerHelper.nonNegativeInt(Integer.toString(Integer.MAX_VALUE)));

		// trimming
		assertEquals(Optional.of(5477), IntegerHelper.nonNegativeInt("\t\t   5477  \n"));

		// empty string
		assertEquals(Optional.empty(), IntegerHelper.nonNegativeInt(""));

		// negatives
		assertEquals(Optional.empty(), IntegerHelper.nonNegativeInt("-1"));
		assertEquals(Optional.empty(), IntegerHelper.nonNegativeInt("-7474"));
		assertEquals(Optional.empty(), IntegerHelper.nonNegativeInt(Integer.toString(Integer.MIN_VALUE)));

		// overflow
		assertEquals(Optional.empty(), IntegerHelper.nonNegativeInt("12345678901"));

		// text
		assertEquals(Optional.empty(), IntegerHelper.nonNegativeInt("Hello world!"));
	}
}