package org.szelagowski.intersection;

import junit.framework.TestCase;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.List;
import java.util.function.Consumer;

import static org.mockito.Mockito.*;

@RunWith(MockitoJUnitRunner.class)
public class IntersectionTesterTest extends TestCase {

	@Mock
	private RandomHelper helper;
	@Mock
	private Intersector intersector;
	@Mock
	private Consumer<IntersectionTester.State> notify;
	@InjectMocks
	private IntersectionTester tester;

	@Test
	public void testTestOnly() {
		var sizeA = 123;
		var sizeB = 456;
		var listA = List.of(1, 2, 3);
		var listB = List.of(2, 3, 4);
		var expected = List.of(3, 4, 5);

		when(helper.<Integer>randomList(eq(sizeA), any())).thenReturn(listA);
		when(helper.<Integer>randomList(eq(sizeB), any())).thenReturn(listB);
		when(intersector.intersection(listA, listB)).thenReturn(expected);

		var result = tester.generateListsAndMeasureIntersectingTime(sizeA, sizeB, notify);

		assertEquals(expected, result.value);
		verify(notify).accept(IntersectionTester.State.GeneratingIntoSetList);
		verify(notify).accept(IntersectionTester.State.GeneratingIterateOverList);
		verify(notify).accept(IntersectionTester.State.Intersecting);
	}

}