package org.szelagowski.intersection;

import junit.framework.TestCase;

import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Random;

public class IntersectorTest extends TestCase {
	private final RandomHelper helper = new RandomHelper();
	private final Intersector intersector = new Intersector();

	public void testIntersection() {
		final var a = List.of(1, 2, 3, 4, 5, 6, 7, 8, 9);
		final var b = List.of(11, 12, 13, 4, 15, 16, 7, 18, 9);
		final List<Integer> result = intersector.intersection(a, b);
		assertEquals(List.of(4, 7, 9), result);
	}

	public void testIntersectionWithEmpty() {
		final var a = List.of(1, 2, 3);
		final List<Integer> emptyA = intersector.intersection(a, Collections.emptyList());
		assertEquals(Collections.emptyList(), emptyA);
		final List<Integer> emptyB = intersector.intersection(Collections.emptyList(), a);
		assertEquals(Collections.emptyList(), emptyB);
		final List<Integer> emptyBoth = intersector.intersection(Collections.emptyList(), a);
		assertEquals(Collections.emptyList(), emptyBoth);
	}

	public void testUsingBuildInFunctions() {
		final var random = new Random();
		final var a = helper.randomList(random.nextInt(1000) + 100, random::nextInt);
		final var b = helper.randomList(random.nextInt(1000) + 100, random::nextInt);
		b.addAll(a.subList(0, random.nextInt(50)));

		final var expectedSet = new HashSet<>(a);
		expectedSet.retainAll(new HashSet<>(b));

		final List<Integer> result = intersector.intersection(a, b);
		assertFalse(result.isEmpty());
		assertEquals(expectedSet, new HashSet<>(result));
	}
}