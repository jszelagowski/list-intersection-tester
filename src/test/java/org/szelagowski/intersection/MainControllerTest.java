package org.szelagowski.intersection;

import javafx.scene.control.*;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;


@RunWith(MockitoJUnitRunner.class)
public class MainControllerTest extends AbstractJavaFxTest {
	@Mock
	private TextField sizeAField;
	@Mock
	private TextField sizeBField;
	@Mock
	private ToggleGroup whichListsRadioGroup;
	@Mock
	private RadioButton radioListA;
	@Mock
	private Button runButton;
	@Mock
	private Label infoLabel;
	@Mock
	private IntersectionTester intersectionTester;

	@InjectMocks
	private MainController controller;

	@Test
	public void testMeasureIntersecting() {
		// todo see README.md - final methods of JavaFX objects disallow mocking!
	}
}