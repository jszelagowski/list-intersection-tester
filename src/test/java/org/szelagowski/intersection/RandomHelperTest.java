package org.szelagowski.intersection;

import junit.framework.TestCase;

import java.util.List;
import java.util.Random;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

public class RandomHelperTest extends TestCase {
	final private Random random = new Random();
	final private RandomHelper helper = new RandomHelper();

	public void testRandomListValues() {
		final List<Integer> expected = IntStream.range(40, 140).boxed().collect(Collectors.toList());
		final var iterator = expected.iterator();
		final List<Integer> result = helper.randomList(expected.size(), iterator::next);
		assertEquals(expected, result);

	}

	public void testRandomList() {
		final var size = random.nextInt(1000) + 100;
		final List<Long> resultLong = helper.randomList(size, random::nextLong);
		assertEquals(size, resultLong.size());
		final List<Double> resultDouble = helper.randomList(size, random::nextDouble);
		assertEquals(size, resultDouble.size());
	}
}