package org.szelagowski.intersection;

import junit.framework.TestCase;

import java.util.Random;

public class TimingTest extends TestCase {

	public void testTimingQuickExecution() {
		final Integer expected = new Random().nextInt();
		var result = Timing.measure(() -> expected);
		assertEquals(expected, result.value);
		assertTrue(result.elapseMs < 100);
	}

	public void testTimingLongExecutions() {
		final Integer expected = new Random().nextInt();
		var result = Timing.measure(() -> {
			try {
				Thread.sleep(100);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
			return expected;
		});
		assertEquals(expected, result.value);
		assertTrue(result.elapseMs >= 100);
		assertTrue(result.elapseMs < 200);
	}
}